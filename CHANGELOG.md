
## [0.0.6] - 10 Aug 2020.
* single file import impl
* bug fixes in logger implementation

## [0.0.5] - 9 Aug 2020.
* added context free toast 
* added context free dialog
* bug fixes in events

## [0.0.4] - 7 Aug 2020.
* added new api to fire event from eventID
* removed debug tag from event Producer

## [0.0.3] - 23 july 2020.
* added more documentation
* added example code

## [0.0.2] - 22 july 2020.

* combined eventid and eventdata for more stream lined code
* child will returned if chilplaceholder is nul just in case you want to build same widget if event is null and fails test

## [0.0.1] - 20 july 2020.

* Eventize your flutter package with it.
