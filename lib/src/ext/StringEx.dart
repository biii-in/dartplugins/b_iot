import 'dart:math';

extension StringEx on String {
  List<int> asUBL(int len, [fill = 0, terminate = true]) =>
      this.substring(0, min(terminate ? len - 1 : len, this.length)).padRight(len, '\0').codeUnits;
}
