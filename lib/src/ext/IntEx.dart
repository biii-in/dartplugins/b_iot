extension IntEx on int {
  List<int> toByteList(bool lsbFirst, int len) {
    List<int> out = List.filled(len, 0);
    if (lsbFirst) {
      for (int i = 0; i < len; i++) {
        out[i] = (this >> (i * 8)) & 0xFF;
      }
    } else {
      for (int i = 0; i < len; i++) {
        out[len - 1 - i] = (this >> (i * 8)) & 0xFF;
      }
    }
    return out;
  }

  static int fromByteList(bool lsbFirst, int len, bool signed, List<int> data) {
    int val = 0;
    if (lsbFirst) {
      len--;
      val = signed ? data[len] : (data[len] & 0xFF);
      while (len > 0) {
        len--;
        val = val << 8;
        val |= (data[len] & 0xFF);
      }
    } else {
      int i = 0;
      val = signed ? data[len] : data[len] & 0xFF;
      while (i < len) {
        i++;
        val = val << 8;
        val |= (data[i] & 0xFF);
      }
    }
    return val;
  }

  List<int> asUBL(int width) {
    int v = this.toUnsigned(width);
    List<int> ubl = [];
    for (int i = 0; i < width; i += 8) {
      ubl.add((v >> i) & 0xFF);
    }
    return ubl;
  }
}
