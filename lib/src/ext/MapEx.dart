extension MapEx<X, Y> on Map<X, Y> {
  Y? GetOr(X key, Y default_value) => containsKey(key) ? this[key] : default_value;

  Map<X, Z> toMapOf<Z>(Z getValue(X key, Y e)) {
    Map<X, Z> newMap = {};
    this.forEach((k, e) => newMap[k] = getValue(k, e));
    return newMap;
  }

  List<Z> toListOf<Z>(Z getValue(X key, Y e)) {
    List<Z> newMap = [];
    this.forEach((k, e) => newMap.add(getValue(k, e)));
    return newMap;
  }
}
