extension IterableEx<T> on Iterable<T> {
  Map<X, T> toMapOf<X>(X getKey(T e, int index)) {
    Map<X, T> map = {};
    int i = 0;
    this.forEach((e) => map.putIfAbsent(getKey(e, i++), () => e));
    return map;
  }

  List<nT> toListOf<nT>(nT getValue(int index, T e)) {
    List<nT> newMap = [];
    int index = 0;
    this.forEach((e) => newMap.add(getValue(index++, e)));
    return newMap;
  }

  List<nT> toListOfRecent<nT>(int n, nT getValue(int index, T e)) {
    List<nT> newMap = [];
    int start = (length - n).clamp(0, length);
    this.skip(start).forEach((e) => newMap.add(getValue(start++, e)));
    return newMap;
  }

  T? firstWhereOrNull(bool Function(T element) test) {
    for (var element in this) {
      if (test(element)) return element;
    }
    return null;
  }
}

extension ListEx<T> on List<T> {
  List<T> recentOfLen(int n) => this.sublist((length - n).clamp(0, length));
}

extension ListIntEx on List<int> {
  int asLInt(int width, int pos) {
    int v = 0;
    for (int i = 0; i < width; i += 8) {
      v |= (this[pos].toUnsigned(8) << i);
    }
    return v;
  }
}
