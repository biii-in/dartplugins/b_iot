import 'dart:typed_data';

final _CMemViewSTM _cMemViewSTM = _CMemViewSTM();

class _CMemViewSTM {
  int idx = 0;
  Endian endian = Endian.little;
}

extension ObjCStream on List<int> {
  ///
  /// helper functions
  ///
  List<int> setEndian(Endian endian) {
    _cMemViewSTM.endian = endian;
    return this;
  }

  get viewIndex => _cMemViewSTM.idx;

  List<int> resetView() {
    _cMemViewSTM.idx = 0;
    return this;
  }

  List<int> rewindBytes({required int len}) {
    _cMemViewSTM.idx < len ? throw Exception('rewind past start') : _cMemViewSTM.idx -= len;
    return this;
  }

  List<int> skipBytes({required int len}) {
    length < _cMemViewSTM.idx + len ? throw Exception('skip past end') : _cMemViewSTM.idx += len;
    return this;
  }

  int csum({required int len, int start = 0}) {
    int end = start + len;
    if (end > length) throw Exception('skip past end');
    int csum = 0;
    for (int i = start; i < end; i++) {
      csum += this[i];
    }
    return csum;
  }

  ///
  /// Decoding routines
  ///

  int nextUInt({required int size}) {
    assert(size >= 0 && size <= 8);
    assert(_cMemViewSTM.idx + size <= length);

    int result = 0;
    if (_cMemViewSTM.endian == Endian.little) {
      for (int i = 0; i < size; i++) {
        result += elementAt(_cMemViewSTM.idx++) << (8 * i);
      }
    } else {
      for (int i = size - 1; i >= 0; i--) {
        result += elementAt(_cMemViewSTM.idx++) << (8 * i);
      }
    }
    return result;
  }

  int nextInt({required int size}) {
    int result = nextUInt(size: size);
    const List<int> MSB = [
      0x80, // size 1
      0x8000, // size 2
      0x800000, // size 3
      0x80000000, // size 4
      0x8000000000, // size 5
      0x800000000000, // size 6
      0x80000000000000, // size 7
      0x8000000000000000, // size 8
    ];
    if ((result & MSB[size - 1]) != 0) {
      result = result - MSB[size - 1];
      result = result - MSB[size - 1];
    }
    return result;
  }

  int nextU8() => nextUInt(size: 1);

  int nextU16() => nextUInt(size: 2);

  int nextU32() => nextUInt(size: 4);

  int nextU64() => nextUInt(size: 8);

  int nextS8() => nextInt(size: 1);

  int nextS16() => nextInt(size: 2);

  int nextS32() => nextInt(size: 4);

  int nextS64() => nextInt(size: 8);

  List<int> nextArrayU8({required int length}) => List.generate(length, (index) => nextU8());

  List<int> nextArrayU16({required int length}) => List.generate(length, (index) => nextU16());

  List<int> nextArrayU32({required int length}) => List.generate(length, (index) => nextU32());

  List<int> nextArrayU64({required int length}) => List.generate(length, (index) => nextU64());

  List<int> nextArrayS8({required int length}) => List.generate(length, (index) => nextS8());

  List<int> nextArrayS16({required int length}) => List.generate(length, (index) => nextS16());

  List<int> nextArrayS32({required int length}) => List.generate(length, (index) => nextS32());

  String nextString({required int length}) => String.fromCharCodes(nextArrayU8(length: length));

  ///
  /// Encoding routines
  ///

  void pushUInt({required int value, required int size}) {
    assert(size >= 0 && size <= 8);

    if (_cMemViewSTM.endian == Endian.little) {
      for (int i = 0; i < size; i++) {
        add((value >> (8 * i)) & 0xFF);
      }
    } else {
      for (int i = size - 1; i >= 0; i--) {
        add((value >> (8 * i)) & 0xFF);
      }
    }
  }

  void pushInt({required int value, required int size}) {
    int result = value;
    if (value < 0) {
      result += 1 << (size * 8);
    }
    pushUInt(value: result, size: size);
  }

  void pushU8({required int value}) => pushUInt(value: value, size: 1);

  void pushU16({required int value}) => pushUInt(value: value, size: 2);

  void pushU32({required int value}) => pushUInt(value: value, size: 4);

  void pushU64({required int value}) => pushUInt(value: value, size: 8);

  void pushS8({required int value}) => pushInt(value: value, size: 1);

  void pushS16({required int value}) => pushInt(value: value, size: 2);

  void pushS32({required int value}) => pushInt(value: value, size: 4);

  void pushS64({required int value}) => pushInt(value: value, size: 8);

  void pushArrayU8({required List<int> values}) => values.forEach((value) => pushU8(value: value));

  void pushArrayU16({required List<int> values}) => values.forEach((value) => pushU16(value: value));

  void pushArrayU32({required List<int> values}) => values.forEach((value) => pushU32(value: value));

  void pushArrayU64({required List<int> values}) => values.forEach((value) => pushU64(value: value));

  void pushArrayS8({required List<int> values}) => values.forEach((value) => pushS8(value: value));

  void pushArrayS16({required List<int> values}) => values.forEach((value) => pushS16(value: value));

  void pushArrayS32({required List<int> values}) => values.forEach((value) => pushS32(value: value));

  void pushString({required String value, required int len}) => pushArrayU8(
        values: List.filled(len, 0)..setRange(0, value.codeUnits.length >= len ? len - 1 : value.codeUnits.length, value.codeUnits),
      );

  void pushPadding({required int len, int value = 0}) => pushArrayU8(values: List.filled(len, value));
}

abstract class ObjCDataInterface<T> {
  T fromList(List<int> data, {Endian endian = Endian.little});

  List<int> toList();
}
