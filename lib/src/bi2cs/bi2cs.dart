import 'package:b_iot/b_iot.dart';
import 'package:b_iot/src/struct/struct.dart';

enum bi2csCmdId {
  config,
  delay,
  waitForVal,
  read,
  write,
  writeBuf,
  compositeRead,
  scan,
  readBuf,
}

enum bi2csRestartMode {
  noRestart,
  restart,
  stopRestart,
}

enum bi2csBusLen {
  byte,
  short,
  _rsv,
  word,
}

abstract class bi2csCmd extends struct {}

extension bi2csCmdList on List<bi2csCmd> {
  List<int> get bytes => this.fold([], (ret, cmd) => ret..addAll(cmd.bytes));
}

class bi2csConfigCmd extends bi2csCmd {
  final uint16_t _hdr;
  //
  late final struct_bits id;
  late final struct_bits i2c_num;
  late final struct_bits addr;
  late final struct_bits msb_first;
  //
  final uint8_t _v;
  late final struct_bits<bi2csRestartMode> write_mode;
  late final struct_bits<bi2csRestartMode> read_mode;
  late final struct_bits<bi2csBusLen> reg_size;
  late final struct_bits<bi2csBusLen> data_size;

  bi2csConfigCmd({
    int i2cNum = 0,
    required int slaveAddr,
    bool msbFirst = false,
    bi2csRestartMode writeMode = bi2csRestartMode.noRestart,
    bi2csRestartMode readMode = bi2csRestartMode.restart,
    bi2csBusLen regSize = bi2csBusLen.byte,
    bi2csBusLen dataSize = bi2csBusLen.byte,
  })  : this._hdr = uint16_t(0),
        this._v = uint8_t(0) {
    id = struct_bits(_hdr, 0, 4)..v = bi2csCmdId.config.index;
    i2c_num = id.next_field(4)..v = i2cNum;
    addr = i2c_num.next_field(7)..v = slaveAddr;
    msb_first = addr.next_field(1)..b = msbFirst;
    //
    write_mode = struct_bits(_v, 0, 2)..e = writeMode;
    read_mode = write_mode.next_field(2)..e = readMode;
    reg_size = read_mode.next_field(2)..e = regSize;
    data_size = reg_size.next_field(2)..e = dataSize;
  }

  @override
  struct packMembers() => pack(_hdr).pack(_v);
}

class bi2csDelayCmd extends bi2csCmd {
  final uint16_t _hdr;
  late final struct_bits id;
  late final struct_bits ms;

  bi2csDelayCmd({required int ms}) : this._hdr = uint16_t(0) {
    this.id = struct_bits(_hdr, 0, 4)..v = bi2csCmdId.delay.index;
    this.ms = id.next_field(12)..v = ms ~/ 10;
  }
  @override
  struct packMembers() => pack(_hdr);
}

typedef bi2csWaitForValR8U8 = bi2csWaitForVal<uint8_t, uint8_t>;
typedef bi2csWaitForValR8U16 = bi2csWaitForVal<uint8_t, uint16_t>;
typedef bi2csWaitForValR8U32 = bi2csWaitForVal<uint8_t, uint32_t>;
typedef bi2csWaitForValR16U8 = bi2csWaitForVal<uint16_t, uint8_t>;
typedef bi2csWaitForValR16U16 = bi2csWaitForVal<uint16_t, uint16_t>;
typedef bi2csWaitForValR16U32 = bi2csWaitForVal<uint16_t, uint32_t>;
typedef bi2csWaitForValR32U8 = bi2csWaitForVal<uint32_t, uint8_t>;
typedef bi2csWaitForValR32U16 = bi2csWaitForVal<uint32_t, uint16_t>;
typedef bi2csWaitForValR32U32 = bi2csWaitForVal<uint32_t, uint32_t>;

class bi2csWaitForVal<RegT extends cint, ValT extends cint> extends bi2csCmd {
  final uint16_t _hdr;
  late final struct_bits id;
  late final struct_bits ms;
  //
  final uint8_t retry;
  final RegT reg;
  final ValT val;
  final ValT mask;

  bi2csWaitForVal({
    required int reg,
    required int val,
    int ms = 10,
    int retry = 5,
    int mask = 0xFFFFFFFF,
  })  : this._hdr = uint16_t(0),
        retry = uint8_t(retry),
        reg = cint.create<RegT>(reg),
        val = cint.create<ValT>(val),
        mask = cint.create<ValT>(mask) {
    this.id = struct_bits<bi2csCmdId>(_hdr, 0, 4)..e = bi2csCmdId.waitForVal;
    this.ms = id.next_field(12)..v = ms ~/ 10;
  }

  @override
  struct packMembers() => pack(_hdr).pack(retry).pack(reg).pack(val).pack(mask);
}

typedef bi2csWriteR8U8 = bi2csWrite<uint8_t, uint8_t_arr>;
typedef bi2csWriteR8U16 = bi2csWrite<uint8_t, uint16_t_arr>;
typedef bi2csWriteR8U32 = bi2csWrite<uint8_t, uint32_t_arr>;
typedef bi2csWriteR16U8 = bi2csWrite<uint16_t, uint8_t_arr>;
typedef bi2csWriteR16U16 = bi2csWrite<uint16_t, uint16_t_arr>;
typedef bi2csWriteR16U32 = bi2csWrite<uint16_t, uint32_t_arr>;
typedef bi2csWriteR32U8 = bi2csWrite<uint32_t, uint8_t_arr>;
typedef bi2csWriteR32U16 = bi2csWrite<uint32_t, uint16_t_arr>;
typedef bi2csWriteR32U32 = bi2csWrite<uint32_t, uint32_t_arr>;

class bi2csWrite<RegT extends cint, ValT extends cint_arr> extends bi2csCmd {
  final uint16_t _hdr;
  late final struct_bits id;
  late final struct_bits len;

  final RegT reg;
  final ValT values;

  bi2csWrite({required int reg, required List<int> values})
      : this._hdr = uint16_t(0),
        this.reg = cint.create<RegT>(reg),
        this.values = cint_arr.wrapValues<ValT>(values) {
    this.id = struct_bits<bi2csCmdId>(_hdr, 0, 4)..e = bi2csCmdId.write;
    this.len = id.next_field(12)..v = values.length;
  }

  @override
  struct packMembers() => pack(_hdr).pack(reg).pack(values);
}

typedef bi2csReadR8 = bi2csRead<uint8_t>;
typedef bi2csReadR16 = bi2csRead<uint16_t>;
typedef bi2csReadR32 = bi2csRead<uint32_t>;

class bi2csRead<RegT extends cint> extends bi2csCmd {
  final uint16_t _hdr;
  late final struct_bits id;
  late final struct_bits len;

  final RegT reg;
  bi2csRead({required int reg, required int len})
      : this._hdr = uint16_t(0),
        this.reg = cint.create<RegT>(reg) {
    this.id = struct_bits(_hdr, 0, 4)..e = bi2csCmdId.read;
    this.len = id.next_field(12)..v = len;
    this.reg.v = reg;
  }

  @override
  struct packMembers() => pack(_hdr).pack(reg);
}

typedef bi2csCompositeReadR8 = bi2csCompositeRead<uint8_t>;
typedef bi2csCompositeReadR16 = bi2csCompositeRead<uint16_t>;
typedef bi2csCompositeReadR32 = bi2csCompositeRead<uint32_t>;

class bi2csCompositeReadStep<RegT extends cint> extends struct {
  final RegT reg;
  final uint16_t len;

  bi2csCompositeReadStep(int reg, int len)
      : reg = cint.create<RegT>(reg),
        len = uint16_t(len);

  @override
  struct packMembers() => pack(reg).pack(len);
}

class bi2csCompositeRead<RegT extends cint> extends bi2csCmd {
  final uint16_t _hdr;
  late final struct_bits id;
  late final struct_bits len;

  final List<bi2csCompositeReadStep> steps;

  bi2csCompositeRead({
    required int len,
    required this.steps,
  }) : this._hdr = uint16_t(0) {
    this.id = struct_bits(_hdr, 0, 4)..e = bi2csCmdId.compositeRead;
    this.len = id.next_field(12)..v = len;
  }

  @override
  struct packMembers() {
    pack(_hdr);
    for (var step in steps) {
      pack(step);
    }
    return this;
  }
}

class bi2csWriteBuf extends bi2csCmd {
  final uint16_t _hdr;
  late final struct_bits id;
  late final struct_bits len;
  late final uint8_t_arr values;

  bi2csWriteBuf({required List<int> values})
      : this._hdr = uint16_t(0),
        this.values = cint_arr.wrapValues<uint8_t_arr>(values) {
    this.id = struct_bits<bi2csCmdId>(_hdr, 0, 4)..e = bi2csCmdId.writeBuf;
    this.len = id.next_field(12)..v = values.length;
  }

  @override
  struct packMembers() => pack(_hdr).pack(values);
}

class bi2csReadBuf extends bi2csCmd {
  final uint16_t _hdr;
  late final struct_bits id;
  late final struct_bits len;
  late final uint8_t_arr buf;

  bi2csReadBuf({required int len}) : this._hdr = uint16_t(0) {
    this.id = struct_bits<bi2csCmdId>(_hdr, 0, 4)..e = bi2csCmdId.readBuf;
    this.len = id.next_field(12)..v = len;
  }

  @override
  struct packMembers() => pack(_hdr);
}
