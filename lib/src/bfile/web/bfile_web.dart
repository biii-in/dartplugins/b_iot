import 'package:b_iot/b_iot.dart';
import 'package:file/memory.dart';
import 'package:file_picker/_internal/file_picker_web.dart';
import 'dart:html' as html;

import '../bfile.dart';

getBFileInstance() => BFileUtilWeb();

class BFileUtilWeb extends BFileUtil {
  @override
  Future<File> createFile(String name, List<int> bytes) =>
      MemoryFileSystem().file(name).writeAsBytes(bytes);

  @override
  Future<void> saveAs(File file, String path) async {
    var blob = html.Blob([await file.readAsBytes()], 'octet/stream');
    html.AnchorElement(href: html.Url.createObjectUrlFromBlob(blob).toString())
      ..setAttribute("download", file.basename)
      ..click();
  }

  @override
  Future<PlatformFile> pickPlatformFile(List<String> allowedExtensions) async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      allowedExtensions: allowedExtensions,
      type: allowedExtensions.isEmpty ? FileType.any : FileType.custom,
      withData: true,
    );
    if (result != null) {
      if (result.isSinglePick) {
        return result.files.first;
      } else {
        return Future.error('Only single file should be selected');
      }
    } else {
      return Future.error('File pick returns will invalid data');
    }
  }
}
