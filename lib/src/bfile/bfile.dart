import 'package:b_iot/b_iot.dart';

import 'native/bfile_io.dart' if (dart.library.html) 'web/bfile_web.dart';

BFileUtil bFiles = getBFileInstance();

abstract class BFileUtil {
  Future<File> createFile(String name, List<int> bytes);

  Future<void> saveAs(File file, String path);

  Future<PlatformFile> pickPlatformFile(List<String> allowedExtensions);
}
