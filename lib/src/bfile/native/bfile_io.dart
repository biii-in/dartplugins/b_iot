import 'dart:io' as io;

import 'package:b_iot/b_iot.dart';
import 'package:file/local.dart';

getBFileInstance() => BFileUtilIO();

class BFileUtilIO extends BFileUtil {
  @override
  Future<File> createFile(String name, List<int> bytes) =>
      const LocalFileSystem().file('${io.Directory.systemTemp.path}/$name').writeAsBytes(bytes);

  @override
  Future<void> saveAs(File file, String path) async {
    throw UnimplementedError();
  }

  @override
  Future<PlatformFile> pickPlatformFile(List<String> allowedExtensions) async {
    throw UnimplementedError();
  }
}
