import 'package:b_iot/b_iot.dart';

final BBleController bleController = BBleController._();

class BBleController extends GetxController {
  ///
  final RxMap<String, BleDeviceController> devicesFound = <String, BleDeviceController>{}.obs;
  RxBool isScanning = false.obs;
  RxBool isReady = false.obs;

  BBleController._() {
    bBle.isReady.listen((isReady) => this.isReady.value = isReady);
  }

  String get errorMessage => bBle.errorMessage;

  void clearScanResult() {
    if (!isScanning.value) {
      devicesFound.clear();
    }
  }

  Future startScan({
    int scanTimeoutSeconds = 30,
    required BBleProfile profile,
    List<BBleDeviceFilter> deviceFilters = const [],
  }) async {
    devicesFound.clear();
    bool scanStarted = await bBle.startScan(
      onDeviceFound: (device) {
        devicesFound.putIfAbsent(
          device.name,
          () => BleDeviceController._(device),
        );
      },
      profile: profile,
      deviceFilters: deviceFilters,
    );
    if ((scanStarted) && (scanTimeoutSeconds > 0)) {
      Future.delayed(
        Duration(seconds: scanTimeoutSeconds),
        () => stopScan(),
      );
    }
    isScanning.value = scanStarted;
  }

  void stopScan() {
    isScanning.value = false;
  }
}

enum BleConnEvents {
  forceConnect,
  fatalError,
  cacheCleared,
  mtuUpdated,
  connParameterUpdated,
  serviceDiscovered
}

class BleDeviceController extends GetxController {
  ///
  final BBleDevice _device;
  bool _isReady = false;
  RxBool get isConnected => _device.isConnected;

  ///
  BleDeviceController._(this._device) {
    _device.onProfileDecoded = (foundServices) => _isReady = foundServices.isNotEmpty;
  }

  ///
  String get name => _device.name;
  String get id => _device.id;
  BBleProfile get profile => _device.profile;

  ///
  Future<bool> connect({
    int connectionTimeoutSeconds = 10,
    int? mtu,
    bool clearGatt = false,
    bool forceReconnect = false,
    void Function(BleConnEvents event, String msg)? events,
  }) async {
    if (_device.isConnected.value) {
      if (forceReconnect) {
        events?.call(BleConnEvents.forceConnect, 'Disconnecting');
        await _device.disconnect();
      } else {
        _device.refreshGatt();
        return true;
      }
    }
    _isReady = false;
    int connectionTimeoutMills = connectionTimeoutSeconds * 1000;
    _device.isConnected.value = await _device.connect(connectionTimeoutSeconds);
    if (_device.isConnected.isTrue) {
      events?.call(BleConnEvents.serviceDiscovered, 'Refreshing GATT');
      while (_isReady == false) {
        await Future.delayed(const Duration(milliseconds: 100));
        connectionTimeoutMills -= 100;
        if (connectionTimeoutMills <= 0) {
          events?.call(BleConnEvents.fatalError, 'Unable to read GATT');
          await _device.disconnect();
          return false;
        }
      }
      events?.call(BleConnEvents.serviceDiscovered, 'Service Found');
      return true;
    } else {
      events?.call(BleConnEvents.fatalError, 'Unable to connect');
      return false;
    }
  }

  Future<void> disconnect() => _device.disconnect();

  BBleService? getSvc(String svc) => _device.profile.services[svc];

  T? getSvcOf<T extends BBleService>(String svc) => _device.profile.getSvcOf<T>();
}
