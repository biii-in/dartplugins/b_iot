import 'package:b_iot/b_iot.dart';

enum _FileOpId {
  create,
  open,
  close,
}

enum _FileModeId {
  _rsv,
  read,
  write,
  read_write,
  append,
  read_append,
}

class _FileOpCmd extends struct {
  // first bytes
  uint8_t handle = uint8_t(0);
  //
  uint8_t _op_mode = uint8_t(0);
  late struct_bits<_FileOpId> op;
  late struct_bits<_FileModeId> mode;
  //
  _FileOpCmd(
    int handle,
    _FileOpId op, {
    _FileModeId mode = _FileModeId.read,
  }) : handle = uint8_t(handle) {
    this.op = struct_bits(_op_mode, 0, 5)..e = op;
    this.mode = this.op.next_field(3)..e = mode;
  }

  @override
  struct packMembers() => pack(handle).pack(_op_mode).pad(2);
}

class _FileInfoCmd extends struct {
  _FileOpCmd cmd;
  uint32_t size;
  uint32_t unix;
  cstr filename;

  _FileInfoCmd(
    int handle,
    _FileOpId op,
    _FileModeId mode,
    String filename, {
    int? unix,
    int size = 0,
  })  : cmd = _FileOpCmd(handle, op, mode: mode),
        size = uint32_t(size ?? 0),
        unix = uint32_t(unix ?? Unix()),
        filename = cstr(filename, 20);

  @override
  struct packMembers() => pack(cmd).pack(size).pack(unix).pack(filename);
}

class _FileReadDataCmd extends struct {
  uint8_t handle;
  uint16_t readLen;
  uint32_t offset;

  _FileReadDataCmd(int handle, int off, int len)
      : handle = uint8_t(handle),
        readLen = uint16_t(len),
        offset = uint32_t(off);

  @override
  struct packMembers() => pack(handle).pad(1).pack(readLen).pack(offset);
}

class _FileWriteDataCmd extends struct {
  uint8_t handle;
  uint32_t offset;
  uint8_t_arr data;

  _FileWriteDataCmd(int handle, int off, List<int> data)
      : offset = uint32_t(off),
        handle = uint8_t(handle),
        data = uint8_t_arr.wrapValues(data);

  @override
  struct packMembers() => pack(offset).pack(handle).pack(data);
}

class BDiscBBleService extends BBleService {
  final fileList = BBleChar('b1117ec4-6109-517c-4f69-939f90485e16',
      'bdisc.file_list', false, true, true);
  final fileCmd = BBleChar('b1117ec4-6209-517c-4f69-939f90485e16',
      'bdisc.file_cmd', true, true, false);
  final fileWrite = BBleChar('b1117ec4-6309-517c-4f69-939f90485e16',
      'bdisc.file_transfer', false, true, false);
  final fileRead = BBleChar('b1117ec4-6409-517c-4f69-939f90485e16',
      'bdisc.file_transfer', false, true, true);

  BDiscBBleService({bool isPrimary = false})
      : super('b1117ec4-6009-517c-4f69-939f90485e16', 'bi2cs', isPrimary) {
    addChar(fileList);
    addChar(fileCmd);
    addChar(fileWrite);
    addChar(fileRead);
  }

  Future<void> openFile({
    required int handle,
    required String filename,
  }) async {
    // open file with write to create it
    var openCmd = _FileInfoCmd(
      handle,
      _FileOpId.open,
      _FileModeId.write,
      filename,
      size: 0,
    );
    return fileCmd.write(openCmd.bytes);
  }

  Future<void> createAndOpenFile({
    required int handle,
    required String filename,
    int size = 0,
  }) async {
    // open file with write to create it
    var createCmd = _FileInfoCmd(
      handle,
      _FileOpId.create,
      _FileModeId.write,
      filename,
      size: size,
    );
    return fileCmd.write(createCmd.bytes);
  }

  Future<void> writeToFile({
    required int handle,
    required List<int> data,
    int offset = 0,
  }) async {
    // send data to write
    var writeCmd = _FileWriteDataCmd(handle, offset, data);
    return fileWrite.write(writeCmd.bytes);
  }

  Future<void> closeFile({required int handle}) async {
    // open file with write to create it
    var closeCmd = _FileOpCmd(handle, _FileOpId.close);
    return fileCmd.write(closeCmd.bytes);
  }
}
