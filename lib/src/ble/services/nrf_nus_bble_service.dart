import 'package:b_iot/src/ble/bble/bble.dart';

class NrfNusBBleService extends BBleService {
  final BBleChar tx = BBleChar(
    '6E400002-B5A3-F393-E0A9-E50E24DCCA9E',
    'nus.tx',
    false,
    true,
    false,
  );
  final BBleChar rx = BBleChar(
    '6E400003-B5A3-F393-E0A9-E50E24DCCA9E',
    'nus.rx',
    false,
    false,
    true,
  );

  NrfNusBBleService({bool isPrimary = false})
      : super(
          '6E400001-B5A3-F393-E0A9-E50E24DCCA9E',
          'nus',
          isPrimary,
        ) {
    addChar(tx);
    addChar(rx);
  }
}
