import 'package:b_iot/b_iot.dart';
import 'package:flutter/foundation.dart';

const Duration _defaultTimeout = const Duration(seconds: 5);

class MCUBootBBleService extends BBleService {
  final BBleChar _smp = BBleChar(
    'da2e7828-fbce-4e01-ae9e-261174997c48',
    'mcuboot.smp',
    false,
    true,
    true,
  );
  Client? _mcumgr;

  MCUBootBBleService({bool isPrimary = false})
      : super(
          '8d53dc1d-1db7-4cd3-868b-8a527460aa84',
          'mcuboot',
          isPrimary,
        ) {
    addChar(_smp);
  }

  void setup({required int mtu}) {
    _mcumgr = Client(
      mtu: mtu,
      input: _smp.notifications!,
      output: (value) => _smp.write(value),
    );
  }

  Future<ImageState> getState({timeout = _defaultTimeout}) =>
      _mcumgr!.readImageState(timeout);

  Future<void> reboot({timeout = _defaultTimeout}) => _mcumgr!.reset(timeout);

  Future<void> testImage(
          {required List<int> hash, timeout = _defaultTimeout}) =>
      _mcumgr!.setPendingImage(hash, false, _defaultTimeout).then((state) {
        var slot = state.getSlotByHash(hash);
        return ((slot != null) && (slot.permanent))
            ? reboot()
            : Future.error(Exception('Invalid Image State $state'));
      });

  Future<void> confirmImage(
          {required List<int> hash, timeout = _defaultTimeout}) =>
      _mcumgr!.setPendingImage(hash, true, _defaultTimeout).then((state) {
        var slot = state.getSlotByHash(hash);
        return ((slot != null) && (slot.permanent))
            ? reboot()
            : Future.error(Exception('Invalid Image State $state'));
      });

  Future<void> confirm({timeout = _defaultTimeout}) =>
      _mcumgr!.confirmImageState(timeout);

  Stream<double> upload({
    required McuImage img,
    required List<int> bin,
    timeout = const Duration(seconds: 120),
    slot = 0,
    window = 1,
  }) {
    StreamController<double> ps = StreamController<double>();
    _mcumgr!
        .uploadImage(slot, bin, img.hash, timeout,
            windowSize: window, onProgress: (step) => ps.add(step / bin.length))
        .then((value) => ps.close())
        .onError((error, stackTrace) {
      ps.addError(error!);
      ps.close();
    });
    return ps.stream;
  }
}

extension ImageStateEx on ImageState {
  ImageStateImage? getSlotById(int slot) =>
      this.images.firstWhereOrNull((slot) => slot.slot == slot);

  ImageStateImage? getSlotByHash(List<int> hash) =>
      this.images.firstWhereOrNull((slot) => listEquals(slot.hash, hash));

  ImageStateImage? getSlot(int slot, List<int> hash) =>
      this.images.firstWhereOrNull(
          (slot) => (slot.slot == slot) && (listEquals(slot.hash, hash)));
}
