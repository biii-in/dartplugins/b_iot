import 'package:b_iot/src/ble/bble/bble.dart';

class BI2csBBleService extends BBleService {
  final BBleChar cmd =
      BBleChar('b1117ec4-41fd-9321-3cea-f88c124d656a', 'bi2cs.cmd', false, true, true);
  final BBleChar data =
      BBleChar('b1117ec4-42fd-9321-3cea-f88c124d656a', 'bi2cs.data', false, false, true);

  BI2csBBleService({bool isPrimary = false})
      : super('b1117ec4-40fd-9321-3cea-f88c124d656a', 'bi2cs', isPrimary) {
    addChar(cmd);
    addChar(data);
  }
}
