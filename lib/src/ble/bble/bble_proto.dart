import 'package:b_iot/b_iot.dart';

typedef CharacteristicChangeCallback = void Function(
    String svcUUID, String charUUID, List<int> data);
typedef StateChangeCallback = void Function(bool isConnected);
typedef BBleProfileDecodedCallback = void Function(List<String> foundServices);
typedef BBleDeviceFoundCallback = void Function(BBleDevice device);

abstract class BBle {
  late BBleDeviceFoundCallback onDeviceFound;

  get errorMessage;

  Stream<bool> get isReady;

  Future<bool> startScan({
    required BBleDeviceFoundCallback onDeviceFound,
    required BBleProfile profile,
    List<BBleDeviceFilter> deviceFilters = const [],
    Duration timeout = const Duration(seconds: 10),
  });

  Future<bool> stopScan();
}

abstract class BBleDevice {
  final String id;
  final String name;
  final BBleProfile profile;
  RxBool isConnected = false.obs;

  BBleProfileDecodedCallback? onProfileDecoded;

  BBleDevice(this.id, this.name, this.profile);

  Future<bool> connect(int connectionTimeoutSeconds);

  Future<void> disconnect();

  void refreshGatt();
}

class BBleDeviceFilter {
  String? nameFull;
  String? namePrefix;
  String? id;
  List<String>? primaryUUID;

  BBleDeviceFilter({
    this.nameFull,
    this.namePrefix,
    this.id,
    this.primaryUUID,
  });

  bool validFilter() => nameFull != null || namePrefix != null || id != null || primaryUUID != null;

  bool validateDevice(String id, String name, List<String> serviceUuids) {
    bool filtersMatched = true;
    if (this.id != null) {
      filtersMatched &= this.id == id;
    }
    if (namePrefix != null) {
      filtersMatched &= name.startsWith(namePrefix!);
    }
    if (nameFull != null) {
      filtersMatched &= name == nameFull;
    }
    if (primaryUUID != null) {
      filtersMatched &= serviceUuids.every((uuid) => primaryUUID!.contains(uuid));
    }
    return filtersMatched;
  }
}

class BBleProfile {
  final String name;
  final Map<String, BBleService> services = {};
  BBleProfile(this.name);

  void addService(BBleService svc) => services[svc.uuid] = svc;

  BBleService? getSvc(String uuid) => services[uuid];

  T? getSvcOf<T extends BBleService>() =>
      services.values.firstWhereOrNull((service) => service is T) as T;
}

class BBleService {
  final String uuid;
  final String name;
  final bool isPrimary;
  final Map<String, BBleChar> chars;
  @protected
  BBleServiceImpl? impl;

  bool get found => impl != null;

  BBleService(String uuid, String name, this.isPrimary, {List<BBleChar> chars = const []})
      : this.uuid = uuid.toLowerCase(),
        this.name = name.toLowerCase(),
        this.chars = chars.toMapOf<String>((e, index) => e.uuid);

  void addChar(BBleChar char) => chars[char.uuid] = char;

  BBleChar? getChar(String uuid) => chars[uuid];

  T? getCharOf<T extends BBleChar>() => chars.values.firstWhereOrNull((char) => char is T) as T;
}

abstract class BBleServiceImpl {
  final dynamic svcObj;
  BBleServiceImpl(this.svcObj);
}

typedef BBleCharUpdateCallback = void Function(List<int> data);

class BBleChar {
  final String uuid;
  final String name;
  final bool notifiable;
  final bool readable;
  final bool writable;
  BBleCharImpl? impl;
  Stream<List<int>>? notifications;

  BBleChar(String uuid, String name, this.readable, this.writable, this.notifiable)
      : this.uuid = uuid.toLowerCase(),
        this.name = name.toLowerCase();

  Future<List<int>> read() => impl!.read();

  Future<void> write(List<int> data, {bool waitForResponse = true}) =>
      impl!.write(data, waitForResponse);
}

abstract class BBleCharImpl {
  final BBleChar pChar;
  final dynamic charObj;

  BBleCharImpl(this.pChar, this.charObj);

  Future<List<int>> read();

  Future<void> write(List<int> data, bool waitForResponse);
}
