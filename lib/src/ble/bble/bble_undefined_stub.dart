import 'bble_proto.dart';

BBle getPlatformSpecificBBleProto() => throw UnsupportedError(
    'BBle Interface is not implemented for this platform');
