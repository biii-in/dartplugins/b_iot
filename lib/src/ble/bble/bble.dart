export 'bble_proto.dart';

import 'bble_proto.dart';
import 'bble_undefined_stub.dart'
    if (dart.library.html) 'web/bble_web_stub.dart'
    if (dart.library.io) 'natives/bble_native_stub.dart';

BBle bBle = getPlatformSpecificBBleProto();
