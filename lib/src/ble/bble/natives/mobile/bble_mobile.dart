import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:b_iot/b_iot.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';

final _fble = FlutterReactiveBle();

class BBleMobile extends BBle {
  StreamSubscription<DiscoveredDevice>? _deviceScanSubscription;

  @override
  get errorMessage => '';

  @override
  Stream<bool> get isReady => _fble.statusStream.map((event) => event == BleStatus.ready);

  void newDeviceFound(BBleDeviceMobile bBleDeviceWeb) {
    Log.i(this, 'newDeviceFound ${bBleDeviceWeb.name}');
    // ignore: deprecated_member_use_from_same_package
    onDeviceFound(bBleDeviceWeb);
  }

  @override
  Future<bool> startScan(
      {required BBleDeviceFoundCallback onDeviceFound,
      required BBleProfile profile,
      List<BBleDeviceFilter> deviceFilters = const [],
      Duration timeout = const Duration(seconds: 10)}) {
    this.onDeviceFound = onDeviceFound;
    _deviceScanSubscription = _fble.scanForDevices(
      withServices: [],
      scanMode: ScanMode.lowLatency,
    ).listen(
      (device) {
        bool valid = false;
        if (deviceFilters.isNotEmpty) {
          for (BBleDeviceFilter filter in deviceFilters) {
            valid |= filter.validateDevice(
              device.id,
              device.name,
              device.serviceUuids.toListOf((e, index) => e.toString()),
            );
          }
        } else {
          valid = true;
        }
        if (valid) {
          newDeviceFound(BBleDeviceMobile(profile, device));
        }
      },
      onError: (e) {
        Log.e(this, '${StackTrace.current} $e');
        return false;
      },
    );
    return Future.value(true);
  }

  @override
  Future<bool> stopScan() async {
    _deviceScanSubscription?.cancel();
    _deviceScanSubscription = null;
    return true;
  }
}

class BBleDeviceMobile extends BBleDevice {
  final DiscoveredDevice _bleDevice;
  StreamSubscription<ConnectionStateUpdate>? _connectedDeviceStream;

  BBleDeviceMobile(BBleProfile profile, this._bleDevice)
      : super(_bleDevice.id, _bleDevice.name, profile);

  bool onServicedDiscovered(List<DiscoveredService> services) {
    List<String> servicesFound = [];
    for (DiscoveredService pSvc in services) {
      if (profile.services[pSvc.serviceId.toString()] != null) {
        profile.services[pSvc.serviceId.toString()]?.impl = BBleServiceImplMobile(pSvc);
        profile.services[pSvc.serviceId.toString()]?.chars.forEach((uuid, char) async {
          try {
            DiscoveredCharacteristic pChar = pSvc.characteristics
                .firstWhere((element) => element.characteristicId.toString() == uuid);
            QualifiedCharacteristic characteristic = QualifiedCharacteristic(
              deviceId: _bleDevice.id,
              serviceId: pSvc.serviceId,
              characteristicId: pChar.characteristicId,
            );
            char.impl = BBleCharImplMobile(char, characteristic);
            if (char.notifiable && (pChar.isNotifiable || pChar.isIndicatable)) {
              char.notifications = _fble.subscribeToCharacteristic(characteristic);
            }
          } catch (e) {
            Log.e(this, 'Error: $e');
          }
        });
        servicesFound.add(pSvc.serviceId.toString());
      } else {
        Log.i(this, 'Unidentified Service ${pSvc.serviceId}');
      }
    }
    onProfileDecoded!(servicesFound);
    Log.i(this, '$name:$id Found service: $servicesFound from $services');
    return servicesFound.isNotEmpty;
  }

  @override
  void refreshGatt() {
    if (isConnected.isTrue) {
      _fble.discoverServices(_bleDevice.id).then(onServicedDiscovered).catchError(
            (e) => Log.e(this, '$name:$id discoverServices error: $e'),
          );
    }
  }

  @override
  Future<bool> connect(int connectionTimeoutSeconds) async {
    DeviceConnectionState _devState = DeviceConnectionState.connecting;
    _connectedDeviceStream = _fble
        .connectToDevice(
      id: _bleDevice.id,
      connectionTimeout: Duration(seconds: connectionTimeoutSeconds),
    )
        .listen(
      (event) {
        if (event.deviceId == _bleDevice.id) {
          if (event.failure != null) {
            Log.i(this, "Error: ${event.failure.toString()}");
            disconnect();
          }
          _devState = event.connectionState;
          switch (_devState) {
            case DeviceConnectionState.connecting:
              break;
            case DeviceConnectionState.disconnecting:
              break;
            case DeviceConnectionState.connected:
              isConnected.value = true;
              break;
            case DeviceConnectionState.disconnected:
              isConnected.value = false;
              break;
          }
        }
      },
      onError: (e) {
        Log.i(this, "Error: $e");
        disconnect();
      },
    );

    ///
    int connectionTimeoutMills = connectionTimeoutSeconds * 1000;
    while ((_devState != DeviceConnectionState.connected) && (_connectedDeviceStream != null)) {
      //
      await Future.delayed(const Duration(milliseconds: 100));
      //
      connectionTimeoutMills -= 100;
      if (connectionTimeoutMills <= 0) {
        disconnect();
      }
      //
      if (_devState == DeviceConnectionState.connected) {
        isConnected.value = true;
        if (Platform.isAndroid) {
          try {
            await _fble.requestConnectionPriority(
                deviceId: _bleDevice.id, priority: ConnectionPriority.highPerformance);
          } catch (e) {
            Log.e(this, 'Error: $e');
            break;
          }
          try {
            await _fble.requestMtu(deviceId: _bleDevice.id, mtu: 512);
          } catch (e) {
            Log.e(this, 'Error: $e');
            break;
          }
          try {
            await _fble.clearGattCache(_bleDevice.id);
          } catch (e) {
            Log.e(this, 'Error: $e');
            break;
          }
        }
        try {
          refreshGatt();
        } catch (e) {
          Log.e(this, 'Error: $e');
          break;
        }
      }
    }
    return isConnected.value;
  }

  Future<void> disconnect() async {
    await _connectedDeviceStream?.cancel();
    _connectedDeviceStream = null;
  }
}

class BBleServiceImplMobile extends BBleServiceImpl {
  BBleServiceImplMobile(svc) : super(svc);
}

class BBleCharImplMobile extends BBleCharImpl {
  BBleCharImplMobile(pChar, char) : super(pChar, char);

  @override
  Future<List<int>> read() async {
    QualifiedCharacteristic char = charObj as QualifiedCharacteristic;
    List<int> data = await _fble.readCharacteristic(char);
    Log.i(this, "CharRea ${pChar.name} is $char : $data");
    return data;
  }

  @override
  Future<void> write(List<int> data, bool waitForResponse) async {
    QualifiedCharacteristic char = charObj as QualifiedCharacteristic;
    Log.i(this, "CharWrite ${pChar.name} is $char : $data");
    return (waitForResponse)
        ? _fble.writeCharacteristicWithResponse(char, value: data)
        : _fble.writeCharacteristicWithoutResponse(char, value: data);
  }
}
