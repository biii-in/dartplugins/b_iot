import '../bble_proto.dart';
import 'dart:io';

import 'mobile/bble_mobile.dart';

BBle getPlatformSpecificBBleProto() => (Platform.isAndroid || Platform.isIOS)
    ? BBleMobile()
    : throw UnsupportedError(
        'Biii Ble interface is not implemented for current platform');
