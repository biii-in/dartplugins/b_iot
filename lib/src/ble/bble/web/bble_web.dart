import 'dart:async';
import 'dart:typed_data';

import 'package:b_iot/b_iot.dart';
import 'package:flutter_web_bluetooth/web_bluetooth_logger.dart';
import 'package:flutter_web_bluetooth/flutter_web_bluetooth.dart';
import 'package:flutter_web_bluetooth/js_web_bluetooth.dart';

class BBleWeb extends BBle {
  bool isConnected = false;

  BBleWeb() : super() {
    initWebBluetoothLogger();
  }

  @override
  Future<bool> startScan({
    required BBleDeviceFoundCallback onDeviceFound,
    required BBleProfile profile,
    List<BBleDeviceFilter> deviceFilters = const [],
    Duration timeout = const Duration(seconds: 10),
  }) async {
    this.onDeviceFound = onDeviceFound;
    if (Bluetooth.hasGetDevices()) {
      Log.i(this, 'getting already paired devices');
      bool done = await Bluetooth.getDevices().then((_bleDevices) {
        Log.i(this, 'got paired devices $_bleDevices');
        for (var _bleDevice in _bleDevices) {
          newDeviceFound(BBleDeviceWeb(profile, BluetoothDevice(_bleDevice)));
        }
        return _bleDevices.isNotEmpty;
      });
      if (done) return done;
    }
    RequestOptionsBuilder optionsBuilder;
    if (deviceFilters.isEmpty) {
      optionsBuilder =
          RequestOptionsBuilder.acceptAllDevices(optionalServices: profile.services.keys.toList());
    } else {
      optionsBuilder = RequestOptionsBuilder(
          deviceFilters.fold<List<RequestFilterBuilder>>(
            [],
            (previousValue, df) => !df.validFilter() ? previousValue : previousValue
              ..add(RequestFilterBuilder(
                name: df.nameFull,
                namePrefix: df.namePrefix,
                services: df.primaryUUID,
              )),
          ),
          optionalServices: profile.services.keys.toList());
    }
    try {
      Log.i(this, 'filter $optionsBuilder');
      return FlutterWebBluetooth.instance.requestDevice(optionsBuilder).then(
        (_bleDevice) {
          newDeviceFound(BBleDeviceWeb(profile, _bleDevice));
          return true;
        },
      ).catchError((e) {
        Log.e(this, '${StackTrace.current} $e');
        return false;
      });
    } on UserCancelledDialogError {
      Log.e(this, '${StackTrace.current} UserCancelledDialogError');
      return false;
    } on DeviceNotFoundError {
      Log.e(this, '${StackTrace.current} DeviceNotFoundError');
      return false;
    } finally {}
  }

  void newDeviceFound(BBleDeviceWeb bBleDeviceWeb) {
    Log.i(this, 'newDeviceFound ${bBleDeviceWeb.name}');
    // ignore: deprecated_member_use_from_same_package
    onDeviceFound(bBleDeviceWeb);
  }

  @override
  String get errorMessage => Bluetooth.isBluetoothAPISupported() ? '' : ' Web Ble is not supported';

  @override
  Stream<bool> get isReady => Bluetooth.getAvailability().asStream();

  @override
  Future<bool> stopScan() async {
    return true;
  }
}

class BBleDeviceWeb extends BBleDevice {
  final BluetoothDevice _bleDevice;

  bool onServicedDiscovered(List<BluetoothService> pSvcs) {
    List<String> pSvcsFound = [];
    for (BluetoothService pSvc in pSvcs) {
      if (profile.services[pSvc.uuid] != null) {
        profile.services[pSvc.uuid]?.impl = BBleServiceImplWeb(pSvc);
        profile.services[pSvc.uuid]?.chars.forEach((uuid, char) async {
          try {
            BluetoothCharacteristic pChar = await pSvc.getCharacteristic(uuid);
            char.impl = BBleCharImplWeb(char, pChar);
            if (char.notifiable && (pChar.properties.notify || pChar.properties.indicate)) {
              if (!pChar.isNotifying) {
                char.notifications = pChar.value.map<List<int>>(
                  (bd) => bd.buffer.asUint8List().toList(),
                );
                if (pChar.isNotifying) {
                  await pChar.stopNotifications();
                }
                await pChar.startNotifications();
              }
            }
          } catch (e) {
            Log.e(this, 'Error: $e');
          }
        });
        pSvcsFound.add(pSvc.uuid);
      } else {
        Log.i(this, 'Unidentified Service ${pSvc.uuid}');
      }
    }
    onProfileDecoded!(pSvcsFound);
    Log.i(this, '$name:$id Found pSvc: $pSvcsFound from $pSvcs');
    return pSvcsFound.isNotEmpty;
  }

  BBleDeviceWeb(BBleProfile profile, this._bleDevice)
      : super(_bleDevice.id, _bleDevice.name ?? '', profile) {
    // _bleDevice.pSvcs.listen(onServicedDiscovered);
    _bleDevice.connected.listen((connected) {
      Log.i(this, '$name:$id connected: $connected');
      isConnected.value = connected;
      refreshGatt();
    });
  }

  @override
  void refreshGatt() {
    if (isConnected.value) {
      _bleDevice.discoverServices().then(onServicedDiscovered).catchError(
            (e) => Log.e(this, '$name:$id discoverServices error: $e'),
          );
    }
  }

  @override
  Future<bool> connect(int connectionTimeoutSeconds) async {
    //
    return _bleDevice.connect(timeout: Duration(seconds: connectionTimeoutSeconds)).then((v) {
      refreshGatt();
      return true;
    }).catchError((e) {
      Log.e(this, '${StackTrace.current} $e');
      return false;
    });
  }

  @override
  Future<void> disconnect() async {
    _bleDevice.disconnect();
  }
}

class BBleServiceImplWeb extends BBleServiceImpl {
  BBleServiceImplWeb(svc) : super(svc);
}

class BBleCharImplWeb extends BBleCharImpl {
  BBleCharImplWeb(pChar, char) : super(pChar, char);

  @override
  Future<List<int>> read() async {
    BluetoothCharacteristic char = charObj as BluetoothCharacteristic;
    ByteData value = await char.readValue();
    List<int> data = value.buffer.asUint8List().toList();
    Log.i(this, "CharRead ${pChar.name} is $char : $data");
    return data;
  }

  @override
  Future<void> write(List<int> data, bool waitForResponse) async {
    BluetoothCharacteristic char = charObj as BluetoothCharacteristic;
    Log.i(this, "CharWrite ${pChar.name} is $char : $data");
    return (waitForResponse)
        ? char.writeValueWithResponse(Uint8List.fromList(data))
        : char.writeValueWithoutResponse(Uint8List.fromList(data));
  }
}
