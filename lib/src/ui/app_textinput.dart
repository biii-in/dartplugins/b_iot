// https://jex.im/regulex/
import 'package:b_iot/b_iot.dart';

const numberRegEx = r'\+?[0-9]{10}';

const doubleRegEx = r'^((\-?[0-9]{1,10}(\.?[0-9]{3})*)|([^\sa-zA-z]+))$';

const intRegEx = r'^((\-?[0-9]{1,10})|([^\sa-zA-z]+))$';

const emailRegEx =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

const nameRegEx =
    r"^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)";

class AppTextInput {
  static Widget emailInput({
    required text,
    Function(String?)? onChanged,
  }) =>
      textInput(
        label: 'Email',
        hint: 'Enter your email',
        text: text,
        regExFormat: emailRegEx,
        onChanged: onChanged,
      );

  static doubleInput({
    required String label,
    required String text,
    String? hint,
    String? info,
    Function(double)? onChanged,
  }) =>
      textInput(
        label: label,
        hint: hint,
        info: info,
        text: text,
        regExFormat: doubleRegEx,
        onChanged: onChanged == null
            ? null
            : (valStr) {
                try {
                  var val = double.parse(valStr!);
                  onChanged(val);
                } catch (e) {}
              },
      );

  static intInput({
    required String label,
    required String text,
    String? hint,
    String? info,
    Function(int)? onChanged,
  }) =>
      textInput(
        label: label,
        hint: hint,
        info: info,
        text: text,
        regExFormat: intRegEx,
        onChanged: onChanged == null
            ? null
            : (valStr) {
                try {
                  var val = int.parse(valStr!);
                  onChanged(val);
                } catch (e) {}
              },
      );

  static Widget textInput({
    required String label,
    required String text,
    int? maxLines,
    String? hint,
    String? info,
    Function(String?)? onChanged,
    String regExFormat = r'[\s\S]',
  }) =>
      _AppTextInput(
        label: label,
        hint: hint,
        info: info,
        text: text,
        regExFormat: regExFormat,
        onChanged: onChanged,
        maxLines: maxLines,
      );
}

class _AppTextInput extends StatelessWidget {
  final String? hint;
  final String? info;
  final String label;
  final String text;
  final int? maxLines;
  final Function(String?)? onChanged;
  final RegExp regEx;
  final TextEditingController _textController;

  _AppTextInput(
      {required this.label,
      required this.text,
      this.hint,
      this.info,
      this.onChanged,
      this.maxLines,
      regExFormat = r'[\s\S]'})
      : regEx = RegExp(regExFormat),
        _textController = TextEditingController(text: text);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        decoration: InputDecoration(
          labelText: label,
          hintText: hint,
          border: const OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          isDense: true,
          prefix: Checkbox(
            value: true,
            onChanged: (bool? value) {},
          ),
          suffixIcon: info == null
              ? null
              : IconButton(
                  icon: Icon(Icons.info),
                  onPressed: () => AppToasts.showToast(info!),
                ),
        ),
        maxLines: maxLines,
        controller: _textController,
        validator: (value) => _defregEx(value ?? ''),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        onChanged: onChanged,
        readOnly: onChanged == null,
        // onEditingComplete: () =>
        //     widget.onChanged?.call(widget._textController.text),
      ),
    );
  }

  _defregEx(String value) {
    bool valid = regEx.hasMatch(value);
    // Log.i(this, 'checking $value against ${widget.regEx.pattern} as $valid');
    return valid ? null : 'Invalid Input';
  }
}
