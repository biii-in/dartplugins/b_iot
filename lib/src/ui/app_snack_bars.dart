import 'package:get/get.dart';

class AppSnackBars {
  static showSnackBar(String title, String message) async {
    // if (Get.isSnackbarOpen) {
    //   // await Get.closeCurrentSnackbar();
    //   Get.closeAllSnackbars();
    // }
    // Get.snackbar(title, message);
    Get.rawSnackbar(title: title, message: message);
  }
}
