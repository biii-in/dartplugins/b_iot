import 'package:b_iot/b_iot.dart';

class AppSwitch {
  static simpleSwitch(
          {required String label,
          required bool value,
          String? hint,
          void Function(bool value)? onChanged}) =>
      _AppSwitch(
        label: label,
        hint: hint,
        initValue: value,
        onChanged: onChanged,
      );
}

class _AppSwitch extends StatefulWidget {
  final String label;
  final String? hint;
  final void Function(bool value)? onChanged;
  final bool initValue;

  _AppSwitch(
      {required this.label,
      required this.initValue,
      this.hint,
      this.onChanged});

  @override
  State<_AppSwitch> createState() => _AppSwitchState(this.initValue);
}

class _AppSwitchState extends State<_AppSwitch> {
  bool curValue;

  _AppSwitchState(this.curValue);

  @override
  Widget build(BuildContext context) => SwitchListTile(
        value: curValue,
        title: Text(widget.label),
        dense: true,
        visualDensity: VisualDensity.compact,
        subtitle: widget.hint == null ? null : Text(widget.hint!),
        onChanged: widget.onChanged == null
            ? null
            : (v) => setState(
                  () {
                    curValue = v;
                    widget.onChanged?.call(v);
                  },
                ),
      );
}
