import 'package:b_iot/b_iot.dart';

class AppDropdown {
  static Widget fromList<T>({
    required label,
    String? hint,
    String? info,
    required T value,
    required List<T> vList,
    Function(T)? onChanged,
  }) =>
      _AppSimpleDropdown<T>(
          label: label,
          hint: hint,
          info: info,
          onChanged: onChanged,
          value: value,
          vMap: vList.toMapOf<String>(
            (e, index) => e.toString(),
          ));

  static Widget fromMap<T>({
    required label,
    String? hint,
    String? info,
    required T value,
    required Map<String, T> vMap,
    Function(T)? onChanged,
  }) =>
      _AppSimpleDropdown<T>(
          label: label, hint: hint, info: info, onChanged: onChanged, value: value, vMap: vMap);
}

class _AppSimpleDropdown<T> extends StatelessWidget {
  final String label;
  final String? hint;
  final String? info;
  final T value;
  final Function(T e)? onChanged;
  final Map<String, T> vMap;

  const _AppSimpleDropdown({
    this.label = '',
    this.hint,
    this.info,
    required this.value,
    required this.onChanged,
    required this.vMap,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ButtonTheme(
        alignedDropdown: false,
        child: DropdownButtonFormField<T>(
          decoration: InputDecoration(
            border: const OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                Radius.circular(8.0),
              ),
            ),
            labelText: label,
            hintText: hint,
            isDense: true,
            // prefixIcon: Icon(Icons.arrow_downward),
            suffixIcon: info == null
                ? null
                : IconButton(
                    icon: Icon(Icons.info),
                    onPressed: () => AppToasts.showToast(info!),
                  ),
          ),
          value: value,
          icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
          selectedItemBuilder: (context) => vMap
              .map(
                (key, value) => MapEntry(
                  key,
                  Row(
                    children: [
                      Icon(Icons.arrow_drop_down),
                      SizedBox(width: 8),
                      Text(key),
                    ],
                  ),
                ),
              )
              .values
              .toList(),
          items: vMap
              .map(
                (key, value) => MapEntry(
                  key,
                  DropdownMenuItem(
                    child: Text(key),
                    value: value,
                  ),
                ),
              )
              .values
              .toList(),
          onChanged: (v) => onChanged?.call(v!),
        ),
      ),
    );
  }
}
