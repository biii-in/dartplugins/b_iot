import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppToasts {
  static showToast(String messsage) {
    Get.rawSnackbar(message: messsage);
  }

  static void showSuccessToast(String messsage) {
    Get.rawSnackbar(
      message: messsage,
      backgroundColor: Colors.green,
      borderRadius: 8,
      borderColor: Colors.green.shade800,
      icon: const Icon(Icons.check, color: Colors.white),
      margin: const EdgeInsets.all(8),
    );
  }

  static void showErrorToast(String messsage) {
    Get.rawSnackbar(
      message: messsage,
      backgroundColor: Colors.red,
      borderRadius: 8,
      borderColor: Colors.red.shade800,
      icon: const Icon(Icons.close, color: Colors.white),
      margin: const EdgeInsets.all(8),
    );
  }
}
