import 'package:b_iot/b_iot.dart';
import 'package:flutter/material.dart';

class AppSliders {
  static simple({
    required String label,
    String? hint,
    required double value,
    double min = 0,
    double max = 100,
    double step = 1,
    void Function(double value)? onChanged,
  }) =>
      Slider(
        value: value,
        onChanged: onChanged,
        label: label,
        min: min,
        max: max,
        divisions: (max - min) ~/ step,
      );

  static range({
    required String label,
    String? hint,
    required RangeValues values,
    double min = 0,
    double max = 100,
    double step = 1,
    int decimalPlaces = 2,
    void Function(RangeValues values)? onChanged,
  }) =>
      Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(label),
          Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: RangeSlider(
                  values: values,
                  onChanged: onChanged,
                  labels: RangeLabels(
                    values.start.toStringAsFixed(decimalPlaces),
                    values.end.toStringAsFixed(decimalPlaces),
                  ),
                  min: min,
                  max: max,
                  divisions: (max - min) ~/ step,
                ),
              ),
              Container(
                child: Text(values.start.toStringAsFixed(decimalPlaces) + ', ' + values.end.toStringAsFixed(decimalPlaces)),
              ),
            ],
          )
        ],
      );
}
