import 'dart:math';

import 'package:b_iot/b_iot.dart';

class AppUtils {
  static Random random = Random();

  static bytesToHex(List<int> data, {String seperator = ''}) => data
      .toListOf((e, index) => e.toRadixString(16).padLeft(2, '0'))
      .join(seperator)
      .toUpperCase();
}
