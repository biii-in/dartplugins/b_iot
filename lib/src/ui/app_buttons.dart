import 'package:async_button_builder/async_button_builder.dart';
import 'package:flutter/material.dart';

class AppButtons {
  static Widget futureButton(String text, Future<void> Function() func) =>
      AsyncButtonBuilder(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20.0,
            vertical: 12.0,
          ),
          child: Text(text, style: const TextStyle(color: Colors.white)),
        ),
        loadingWidget: const Padding(
          padding: EdgeInsets.all(8.0),
          child: SizedBox(
            height: 20.0,
            width: 20.0,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
        ),
        successWidget: const Padding(
          padding: EdgeInsets.all(4.0),
          child: Icon(
            Icons.check,
            size: 20,
            color: Colors.purpleAccent,
          ),
        ),
        onPressed: func,
        loadingSwitchInCurve: Curves.bounceInOut,
        loadingTransitionBuilder: (child, animation) {
          return SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(0, 1.0),
              end: const Offset(0, 0),
            ).animate(animation),
            child: child,
          );
        },
        builder: (context, child, callback, state) {
          return Material(
            color: state.maybeWhen(
              success: () => Colors.purple[100],
              orElse: () => Colors.blue,
            ),
            // This prevents the loading indicator showing below the
            // button
            clipBehavior: Clip.hardEdge,
            shape: const StadiumBorder(),
            child: InkWell(
              child: child,
              onTap: callback,
            ),
          );
        },
      );
}
