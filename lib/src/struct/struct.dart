import 'package:b_iot/b_iot.dart';

abstract class ctype<V> {
  int _off = 0;
  int _len = 0;
  //
  ctype(this._len);

  List<int> get bytes;
  set bytes(List<int> buf);

  int get structSize => _len;
}

class cbool extends ctype<bool> {
  bool v;
  cbool(this.v) : super(1);
  @override
  List<int> get bytes => [v ? 1 : 0];

  @override
  set bytes(List<int> buf) => v = (buf[_off] != 0);
}

class _struct_pad extends ctype {
  @override
  List<int> bytes;

  _struct_pad(super.len) : bytes = List.filled(len, 0xFF);
}

abstract class struct extends ctype {
  List<ctype> _fields = [];

  struct() : super(0) {
    packMembers();
  }

  struct pack(ctype f) {
    f._off = _len;
    _len += f._len;
    _fields.add(f);
    return this;
  }

  struct pad(int len) => pack(_struct_pad(len));

  @protected
  struct packMembers();

  List<int> get bytes => _fields.fold([], (p, f) => p..addAll(f.bytes));

  set bytes(List<int> buf) => _fields.forEach((f) {
        if (f is struct) {
          f.bytes = buf.sublist(f._off);
        } else {
          f.bytes = buf;
        }
      });
}

abstract class cint<E> extends ctype<int> {
  int v;

  cint(this.v, len) : super(len);

  static T create<T extends cint>(int init) {
    switch (T) {
      case uint32_t:
        return uint32_t(init) as T;
      case uint16_t:
        return uint16_t(init) as T;
      case uint8_t:
      default:
        return uint8_t(init) as T;
    }
  }

  E get e => (E as dynamic).values[v];

  set e(E e) => v = (e as Enum).index;
}

class int8_t<E> extends cint<E> {
  int8_t(int v) : super(v, 1);
  @override
  List<int> get bytes => v.asUBL(8);

  @override
  set bytes(List<int> buf) => v = buf.asLInt(8, _off).toSigned(8);
}

class int16_t<E> extends cint<E> {
  int16_t(int v) : super(v, 2);
  @override
  List<int> get bytes => v.asUBL(16);

  @override
  set bytes(List<int> buf) => v = buf.asLInt(16, _off).toSigned(16);
}

class int32_t<E> extends cint<E> {
  int32_t(int v) : super(v, 4);
  @override
  List<int> get bytes => v.asUBL(32);

  @override
  set bytes(List<int> buf) => v = buf.asLInt(32, _off).toSigned(32);
}

class uint8_t<E> extends cint<E> {
  uint8_t(int v) : super(v, 1);
  @override
  List<int> get bytes => v.asUBL(8);

  @override
  set bytes(List<int> buf) => v = buf.asLInt(8, _off);
}

class uint16_t<E> extends cint<E> {
  uint16_t(int v) : super(v, 2);
  @override
  List<int> get bytes => v.asUBL(16);

  @override
  set bytes(List<int> buf) => v = buf.asLInt(16, _off);
}

class uint32_t<E> extends cint<E> {
  uint32_t(int v) : super(v, 4);
  @override
  List<int> get bytes => v.asUBL(32);

  @override
  set bytes(List<int> buf) => v = buf.asLInt(32, _off);
}

class cstr extends ctype<String> {
  String v;
  final int _fill;
  final bool _terminate;

  cstr(
    this.v,
    len, [
    this._fill = 0,
    this._terminate = true,
  ]) : super(len);

  @override
  List<int> get bytes => v.asUBL(_len, _fill, _terminate);

  @override
  set bytes(List<int> buf) => v = '';
}

abstract class cint_arr<U extends TypedData> extends ctype<U> {
  late U v;

  cint_arr(len) : super(len);

  static T wrapValues<T extends cint_arr>(List<int> data) {
    switch (T) {
      case uint8_t:
      default:
        return uint8_t_arr.wrapValues(data) as T;
    }
  }

  get bytes => v.buffer.asUint8List();
}

class uint8_t_arr extends cint_arr<Uint8List> {
  uint8_t_arr(super.len) {
    v = Uint8List(_len);
  }
  uint8_t_arr.wrapValues(List<int> data) : super(data.length) {
    bytes = data;
  }

  @override
  set bytes(List<int> buf) => v = Uint8List.fromList(buf);
}

class int16_t_arr extends cint_arr<Int16List> {
  int16_t_arr(len) : super(len * 2) {
    v = Int16List(_len);
  }

  int16_t_arr.wrapValues(List<int> data) : super(data.length) {
    bytes = data;
  }

  @override
  set bytes(List<int> buf) => v = Uint8List.fromList(buf).buffer.asInt16List();
}

class uint16_t_arr extends cint_arr<Uint16List> {
  uint16_t_arr(len) : super(len * 2) {
    v = Uint16List(_len);
  }

  uint16_t_arr.wrapValues(List<int> data) : super(data.length) {
    bytes = data;
  }

  @override
  set bytes(List<int> buf) => v = Uint8List.fromList(buf).buffer.asUint16List();
}

class uint32_t_arr extends cint_arr<Uint32List> {
  uint32_t_arr(len) : super(len * 4) {
    v = Uint32List(_len);
  }

  uint32_t_arr.wrapValues(List<int> data) : super(data.length) {
    bytes = data;
  }

  @override
  set bytes(List<int> buf) => v = Uint8List.fromList(buf).buffer.asUint32List();
}

class struct_bits<E> {
  final int _bit_pos;
  final int _bit_len;
  final int _bit_mask;
  final cint _v;
  const struct_bits(this._v, this._bit_pos, this._bit_len)
      : _bit_mask = (((1 << _bit_len) - 1) << _bit_pos);

  int get end => _bit_pos + _bit_len;

  int get v => ((_v.v & _bit_mask) >> _bit_pos);

  set v(int v) {
    _v.v = (_v.v & ~_bit_mask);
    _v.v |= ((v << _bit_pos) & _bit_mask);
  }

  bool get b => v == 1;

  set b(bool b) => v = b ? 1 : 0;

  E get e => (E as dynamic).values[v];

  set e(E e) => v = (e as Enum).index;

  struct_bits<T> next_field<T>(int bit_len) => struct_bits(_v, end, bit_len);
}
