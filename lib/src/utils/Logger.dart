import 'package:flutter/material.dart';

const int kLogDebugLevel = Log.Info;

class Log {
  static const int Error = 0;
  static const int Debug = 1;
  static const int Info = 2;
  // static final _logger = Logger('BIoT');

  /// return [tag] if it is string else return type of tag as string
  static String _getTag(tag) => (tag.runtimeType == String ? tag : tag.runtimeType).toString();

  /// return [msg] if it is string else calls toString method
  static String _getMsg(msg) => msg.runtimeType == String ? msg : msg.toString();

  /// returns current formatted time
  static String _curTime() => DateTime.now().toString();

  /// static method to log debug logs
  /// format is simple time, type of [tag] or [tag] if it is string, message [msg]
  static e(dynamic tag, dynamic msg) {
    // _logger.shout('${_getTag(tag)}: ${_getMsg(msg)}');
    debugPrint('${_getTag(tag)}: ${_getMsg(msg)}');
  }

  /// static method to log debug logs
  /// format is simple time, type of [tag] or [tag] if it is string, message [msg]
  static d(dynamic tag, dynamic msg) {
    // _logger.warning('${_getTag(tag)}: ${_getMsg(msg)}');
    debugPrint('${_getTag(tag)}: ${_getMsg(msg)}');
  }

  /// static method to log debug logs
  /// format is simple time, type of [tag] or [tag] if it is string, message [msg]
  static i(dynamic tag, dynamic msg) {
    // _logger.info('${_getTag(tag)}: ${_getMsg(msg)}');
    debugPrint('${_getTag(tag)}: ${_getMsg(msg)}');
  }
}
