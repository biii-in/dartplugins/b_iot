import 'dart:io';

import 'package:b_iot/b_iot.dart';
import 'package:bble_tools/page/ble_device_actions_page.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class PrepareBleProfile extends StatefulWidget {
  const PrepareBleProfile({super.key});

  @override
  State<PrepareBleProfile> createState() => _PrepareBleProfileState();
}

class _PrepareBleProfileState extends State<PrepareBleProfile> {
  List<BBleService> knownServices = [
    NrfNusBBleService(),
    MCUBootBBleService(),
    BI2csBBleService(),
    BDiscBBleService(),
  ];
  BBleProfile profile = BBleProfile('Custom');
  bool connecting = false;

  @override
  void initState() {
    super.initState();
    knownServices.forEach((svc) => profile.addService(svc));
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: const Text('Build Custom Profile'),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            if (kIsWeb) {
              bleController.devicesFound.listen((d) {
                if (d.values.isNotEmpty) {
                  setState(() => connecting = true);
                  var dev = d.values.first;
                  dev
                      .connect(mtu: 517, connectionTimeoutSeconds: 20)
                      .then((connected) {
                    print('Connected: $connected');
                    bleController.devicesFound.clear();
                    if (connected) {
                      Get.to(BleDeviceActionsPage(dev));
                    }
                    setState(() => connecting = false);
                  }).catchError((e) {
                    print('error $e');
                    setState(() => connecting = false);
                  });
                }
              });
            } else {
              if (Platform.isAndroid) {
                //
              } else if (Platform.isIOS) {
                //
              }
            }
            bleController.startScan(
              profile: profile,
            );
            // }
          },
          label: const Text('Connect'),
          icon: const Icon(Icons.navigate_next),
        ),
        body: connecting
            ? const Center(
                child: Column(children: [
                Text('Connecting'),
                CircularProgressIndicator()
              ]))
            : Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: ListView.builder(
                      itemCount: knownServices.length,
                      itemBuilder: (c, i) => ListTile(
                        leading: Icon(
                            profile.services.containsKey(knownServices[i].uuid)
                                ? Icons.check_box_outlined
                                : Icons.check_box_outline_blank),
                        title: Text(knownServices[i].name),
                        subtitle: Text(knownServices[i].uuid),
                        onTap: () => setState(() {
                          if (profile.services
                              .containsKey(knownServices[i].uuid)) {
                            profile.services.remove(knownServices[i].uuid);
                          } else {
                            profile.addService(knownServices[i]);
                          }
                        }),
                      ),
                    ),
                  ),
                ],
              ),
      );
}
