import 'package:b_iot/b_iot.dart';
import 'package:archive/archive.dart';

class MCUBootServicePage extends StatefulWidget {
  final BleDeviceController device;
  final MCUBootBBleService mcu;
  const MCUBootServicePage(this.device, this.mcu, {super.key});

  @override
  State<MCUBootServicePage> createState() => _MCUBootServicePageState();
}

class _MCUBootServicePageState extends State<MCUBootServicePage> {
  bool actionPending = false;
  double? actionProgress;
  String actionText = '';
  ImageState? imgState;

  @override
  void initState() {
    super.initState();
    widget.mcu.setup(mtu: 27);
    refreshState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: const Text('MCUBoot'),
          actions: [
            IconButton(
              icon: const Icon(Icons.restart_alt_rounded),
              onPressed: () => widget.mcu.reboot().then(
                    (value) => Navigator.of(context).pop(),
                  ),
            ),
            IconButton(
              icon: const Icon(Icons.refresh),
              onPressed: actionPending ? null : refreshState,
            ),
          ],
        ),
        body: actionPending
            ? Column(children: [
                Text(actionText),
                CircularProgressIndicator(
                  value: actionProgress,
                )
              ])
            : imgState == null
                ? const Text('Please Refresh Image state')
                : Column(
                    children: imgState!.images.toListOf(
                      (_, img) => Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Slot ID: ${img.slot}'),
                                Text('Version: ${img.version}'),
                                Text(
                                    'Hash: ${hex.encode(img.hash).toUpperCase()}'),
                                Text('Bootable: ${img.bootable}'),
                                Text('Active: ${img.active}'),
                                Text('Confirmed: ${img.confirmed}'),
                                Text('Pending: ${img.pending}'),
                                Text('Permanent: ${img.permanent}'),
                                if (img.slot == 0) ...[
                                  if (!img.confirmed) ...[
                                    MaterialButton(
                                      child: const Text('Confirm'),
                                      onPressed: () => widget.mcu
                                          .confirm()
                                          .then((v) => refreshState()),
                                    )
                                  ]
                                ],
                                if ((img.slot > 0) && (!img.confirmed)) ...[
                                  if (!img.pending) ...[
                                    MaterialButton(
                                      child: const Text('Test'),
                                      onPressed: () => widget.mcu
                                          .testImage(hash: img.hash)
                                          .then((v) => refreshState()),
                                    )
                                  ],
                                  if (!img.permanent) ...[
                                    MaterialButton(
                                      child: const Text('Confirm'),
                                      onPressed: () => widget.mcu
                                          .confirmImage(hash: img.hash)
                                          .then((v) => refreshState()),
                                    )
                                  ],
                                  MaterialButton(
                                    child: const Text('Upload'),
                                    onPressed: () => uploadImage(img.slot),
                                  )
                                ]
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
      );

  void refreshState() async {
    actionProgress = null;
    actionText = 'Reading Image State';
    setState(() => actionPending = true);
    await widget.mcu.getState().then((state) {
      imgState = state;
    }).catchError((err) {
      print('error $err');
    });
    setState(() => actionPending = false);
  }

  void uploadImage(int slot) async {
    var file = await bFiles.pickPlatformFile(['zip', 'bin']);
    List<int> bin = file.bytes!;
    if (file.name.endsWith('.zip')) {
      // extract bin file
      InputStream ifs = InputStream(bin);
      final archive = ZipDecoder().decodeBuffer(ifs);
      var appBinZip = archive.findFile('app_update.bin');
      print('app_update.bin == ${appBinZip?.size}');
      bin = appBinZip?.content as List<int>;
    }
    final img = McuImage.decode(bin);
    actionProgress = null;
    actionText = 'Starting Upload...';
    setState(() => actionPending = true);
    widget.mcu.upload(img: img, bin: bin).listen((progress) {
      setState(() {
        actionText = progress > 0 ? 'Uploading...' : 'Erasing Slot: $slot...';
        actionProgress = progress;
      });
    })
      ..onDone(() {
        setState(() {
          actionPending = false;
          AppToasts.showSuccessToast('Upload Done');
        });
      })
      ..onError((e) {
        setState(() {
          actionPending = false;
          AppToasts.showSuccessToast('Upload Error: $e');
        });
      });
  }
}
