import 'package:b_iot/b_iot.dart';
import 'package:xterm/xterm.dart';

class NrfNusTerminalPage extends StatefulWidget {
  final BleDeviceController device;
  final NrfNusBBleService nus;
  const NrfNusTerminalPage(this.device, this.nus, {super.key});

  @override
  State<NrfNusTerminalPage> createState() => _NrfNusTerminalPageState();
}

class _NrfNusTerminalPageState extends State<NrfNusTerminalPage> {
  final TextEditingController inpCtrl = TextEditingController();
  late final Terminal terminal;

  String ending = '\n';
  String txLine = '';
  String rxLine = '';

  @override
  void initState() {
    super.initState();
    widget.nus.rx.notifications?.listen((data) {
      rxLine = String.fromCharCodes(data);
      terminal.write(rxLine);
      setState(() {});
    });
    terminal = Terminal(onOutput: (data) {
      txLine += data;
      if (data.endsWith('\x0d')) {
        terminal.write('\r\n');
        txLine += ending;
        widget.nus.tx.write(txLine.codeUnits);
        txLine = '';
      } else {
        terminal.write(data);
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: const Text('NUS Terminal'),
          actions: [
            PopupMenuButton(
              initialValue: ending,
              itemBuilder: (context) =>
                  {'LF': '\n', 'CRLF': '\r\n', 'None': ''}.toListOf(
                (key, val) => PopupMenuItem(
                  value: val,
                  child: Text(key),
                  onTap: () => setState(() => ending = val),
                ),
              ),
              child: const Text('LineFeed'),
            ),
          ],
        ),
        body: TerminalView(terminal),
      );
}
