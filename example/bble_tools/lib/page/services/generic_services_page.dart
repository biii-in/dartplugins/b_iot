import 'package:b_iot/b_iot.dart';

class GenericServicesPage extends StatefulWidget {
  final BleDeviceController device;
  final BBleService svc;
  const GenericServicesPage(this.device, this.svc, {super.key});

  @override
  State<GenericServicesPage> createState() => _GenericServicesPageState();
}

class _GenericServicesPageState extends State<GenericServicesPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('Build Custom Profile'),
      ),
      body: Column());
}
