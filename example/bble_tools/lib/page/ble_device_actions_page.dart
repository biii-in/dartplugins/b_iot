import 'package:b_iot/b_iot.dart';
import 'package:bble_tools/page/services/generic_services_page.dart';
import 'package:bble_tools/page/services/mcuboot/mcuboot_service_page.dart';
import 'package:bble_tools/page/services/nus/nrf_nus_page.dart';

class _ProfileUISettings {
  final IconData icon;
  final Widget Function(BleDeviceController device, BBleService svc) inst;
  _ProfileUISettings(this.icon, this.inst);
}

final Map<Type, _ProfileUISettings> _ui = {
  NrfNusBBleService: _ProfileUISettings(
    Icons.terminal,
    (device, svc) => NrfNusTerminalPage(device, svc as NrfNusBBleService),
  ),
  MCUBootBBleService: _ProfileUISettings(
    Icons.system_update,
    (device, svc) => MCUBootServicePage(device, svc as MCUBootBBleService),
  ),
};

class BleDeviceActionsPage extends StatefulWidget {
  final BleDeviceController device;

  const BleDeviceActionsPage(this.device, {super.key});

  @override
  State<BleDeviceActionsPage> createState() => _BleDeviceActionsPageState();
}

class _BleDeviceActionsPageState extends State<BleDeviceActionsPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: const Text('Supported Profile Actions'),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: widget.device.profile.services.values
              .where((svc) => svc.found)
              .toListOf<Widget>(
                (_, svc) => ListTile(
                  leading: Icon(_ui[svc.runtimeType]?.icon ??
                      Icons.miscellaneous_services),
                  title: Text(svc.name),
                  subtitle: Text(svc.uuid),
                  onTap: () => Get.to(
                    _ui[svc.runtimeType]?.inst(widget.device, svc) ??
                        GenericServicesPage(widget.device, svc),
                  ),
                ),
              ),
        ),
      );
}
